{-# LANGUAGE FlexibleInstances #-}

import Control.Monad
import Control.Concurrent

newtype SharedState s m a = SharedState { runSState :: MVar s -> m a}

type SharedStateIO s = SharedState s IO

instance Applicative (SharedStateIO s) where
  pure = return
  (<*>) = ap

instance Functor (SharedStateIO s) where
  fmap = liftM

instance Monad (SharedStateIO s) where
  return x = SharedState $ \mv ->  do
    s <- readMVar mv
    return x

  m >>= f = SharedState $ \mv -> do
    a <- runSState m mv
    runSState (f a) mv

update :: s -> SharedStateIO s ()
update s = SharedState $ \mv -> do
  swapMVar mv s
  return ()

get :: SharedStateIO s s
get = SharedState $ \mv -> readMVar mv

incCounter :: SharedStateIO Int Int
incCounter = do
  counter <- get
  update (counter + 1)
  return counter

decCounter :: SharedStateIO Int Int
decCounter = do
  counter <- get
  update (counter - 1)
  return counter

main :: IO ()
main = do
  mv <- newMVar 10

  forkIO (replicateM_ 20 $ do
    count <- runSState incCounter mv
    print count)

  replicateM_ 20 $ do
    count <- runSState decCounter mv
    print count
