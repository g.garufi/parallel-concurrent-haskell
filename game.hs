import Control.Concurrent
import System.Random
import Text.Printf
import System.IO
import System.Timeout
import Control.Monad

data Msg = C Char | Time

instance Show Msg where
  show (C c) = show c
  show Time = "Time"


main = do
  hSetBuffering stdout NoBuffering
  hSetBuffering stdin NoBuffering
  hSetEcho stdin False

  mv <- newEmptyMVar
  score <- newMVar 0

  forkIO (forever $ do c <- getChar; putMVar mv (C c))

  forkIO (forever $ do threadDelay (10^6); putMVar mv Time)

  loop mv ""
   where
     update old new = do
       putStr (replicate n '\8')
       putStr (replicate n ' ')
       putStr (replicate n '\8')
       putStr new
      where n = length old

     loop mv s = do
       evt <- takeMVar mv
       case evt of
         C c -> do
           let new = remove c s
           update s new
           loop mv new

         Time -> do
           newDigit <- randomRIO ('0', '9')
           let new = s ++ [newDigit]
           update s new
           loop mv new

       where
         remove x xs = filter (/=x) xs
