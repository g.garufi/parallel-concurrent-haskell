{-# LANGUAGE DeriveDataTypeable, DeriveGeneric, TemplateHaskell #-}

module Dbworker (Request(..), Key, Value, db) where

import Data.Typeable
import Text.Printf
import GHC.Generics
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Binary (Binary)
import Control.Distributed.Process
import Control.Distributed.Process.Closure



data Request = Get Key (SendPort (Maybe Value))
             | Set Key Value
    deriving (Typeable, Generic)

instance Binary Request

type Key   = String
type Value = String


db :: Process ()
db = go Map.empty
  where
    go m = do
      msg <- expect
      case msg of
        Set k v -> do
          say $ printf "setting %s = %s" k v
          go $ Map.insert k v m
        Get k chan -> do
           say $ printf "Getting %s" k
           let v = Map.lookup k m
           sendChan chan v
           go m

--remotable ['db]
