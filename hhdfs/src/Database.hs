{-# LANGUAGE TemplateHaskell #-}

module Database (
       Database,
       Key, Value,
       createDB,
       get, set,
       myRemoteTable
  ) where

import qualified Data.Map as Map
import Text.Printf
import System.IO.Unsafe
import Data.List (partition)
import Data.Char
import Data.Map (Map)
import Control.Concurrent.STM
import Control.Monad (forever, forM)
import Network.Transport.TCP (createTransport, defaultTCPParameters)
import Control.Distributed.Process
import Control.Distributed.Process.Node
import Control.Distributed.Process.Closure

import Dbworker

import Debug.Trace

type Database = ProcessId

remotable ['db]

myRemoteTable :: RemoteTable
myRemoteTable = Database.__remoteTable initRemoteTable

set :: Database -> Key -> Value -> Process ()
set db k v = send db (Set k v)

get :: Database -> Key -> Process (Maybe Value)
get db k = do
  self <- getSelfPid
  -- say $ printf "I am %s, forwarding get: '%s' command" (show self) k
  (sendport,recvport) <- newChan
  send db (Get k sendport)
  v <- receiveChan recvport
  -- say $ printf "got %s" (show v)
  return v

createDB :: [NodeId] -> Process Database
createDB nids = do
  ps <- forM nids $ \nid -> do
     say $ printf "initializing %s" (show nid)
     spawn nid $(mkStaticClosure 'db)

  lpid <- spawnLocal $ listener ps
  spawnLocal $ watchDog ps lpid

  return lpid

watchDog :: [ProcessId] -> Database -> Process ()
watchDog pids db = do
  mapM_ monitor pids
  forever $ do
    ProcessMonitorNotification ref pid reason <- expect
    -- say (printf "process %s died: %s" (show pid) (show reason))
    send db (ProcessMonitorNotification ref pid reason)

listener :: [ProcessId] -> Process ()
listener pids = uncurry loop $ pairUp (map Just pids)
  where
    loop :: [Maybe ProcessId] -> [Maybe ProcessId] -> Process ()
    loop odds evens = receiveWait
        [ match $ \req -> do
            handleRequest odds evens req
            loop odds evens
        , match $ \(ProcessMonitorNotification ref pid reason) -> do
            say (printf "process %s died: %s" (show pid) (show reason))
            loop (fmap (matches pid) odds) (fmap (matches pid) evens)
        ]

    handleRequest :: [Maybe ProcessId] -> [Maybe ProcessId] -> Request -> Process ()
    handleRequest odds evens r =
      case r of
        Set k _ -> do
          mbSend odds k r
          mbSend evens k r
        Get k _ -> do
          mbSend odds k r
          mbSend evens k r

mbSend :: [Maybe ProcessId] -> Key -> Request -> Process ()
mbSend pids k r =
  case toPid pids k of
    Nothing -> return ()
    Just p -> send p r

toPid :: [Maybe ProcessId] -> String -> Maybe ProcessId
toPid pids s = pids !! (ord (head s) `mod` length pids)

matches :: ProcessId -> Maybe ProcessId -> Maybe ProcessId
matches pid x = case fmap (== pid) x of
  Nothing -> Nothing
  Just True -> Nothing
  Just False -> x

pairUp :: [a] -> ([a], [a])
pairUp [] = ([], [])
pairUp [x] = ([x], [])
pairUp (x:y:xs) = (x:xp, y:yp) where (xp, yp) = pairUp xs
