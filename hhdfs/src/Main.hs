import Control.Concurrent (threadDelay)
import Control.Monad (forever, zipWithM_, unless, forM, forM_)
import Network.Transport.TCP (createTransport, defaultTCPParameters)
import Text.Printf
import Data.Char
import System.IO
import Control.Distributed.Process
import Control.Distributed.Process.Node (runProcess)
import Control.Distributed.Process.Backend.SimpleLocalnet
--import Control.Distributed.Process.Closure
import System.Environment (getArgs)
import Database  (Database, createDB, get, set, myRemoteTable)

main :: IO ()
main = do
  args <- getArgs

  case args of
   ["master", host, port] -> do
     backend <- initializeBackend host port myRemoteTable
     startMaster backend (master backend)
   ["slave", host, port] -> do
     backend <- initializeBackend host port myRemoteTable
     startSlave backend


master :: Backend -> [NodeId] -> Process ()
master backend peers = do
  db <- createDB peers

  f <- liftIO $ readFile "./src/Database.hs"
  let ws = words f

  zipWithM_ (set db) ws (tail ws)
  modName <- get db "module"
  say $ printf "Got %s" (show modName)

  loop db

  where
  loop db = do
    l <- liftIO $ do putStr "key: "; hFlush stdout; getLine
    if l == ":q"
      then terminateAllSlaves backend
      else do
        r <- get db l
        say $ printf "Got %s" (show r)
        loop db
